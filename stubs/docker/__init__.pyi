from typing import Dict, Union

from .models.networks import NetworkCollection
from .models.containers import ContainerCollection
from .models.images import ImageCollection

class TLSConfig: ...

class DockerClient:
    networks: NetworkCollection
    containers: ContainerCollection
    images: ImageCollection
    def __init__(
        self,
        base_url: str,
        version: str,
        timeout: int,
        tls: Union[bool, TLSConfig],
        user_agent: str,
        credstore_env: Dict[str, str],
        use_ssh_client: bool,
        max_pool_size: int,
    ): ...

def from_env() -> DockerClient: ...
