from typing import Optional, Union, TypedDict, Literal, List, Dict, Tuple

class PortAttrs(TypedDict):
    HostPort: str

class Container:
    name: str
    ports: Dict[str, List[PortAttrs]]
    status: Literal["created", "restarting", "running", "removing", "paused", "exited", "dead"]
    def start(self, interactive: bool = ...) -> None: ...
    def stop(self, timeout: int = ...) -> None: ...
    def kill(self, signal: Union[str, int] = ...) -> None: ...
    def reload(self) -> None: ...
    def wait(self) -> None: ...
    def remove(self) -> None: ...

class VolumeConfig(TypedDict, total=False):
    bind: str
    mode: Literal["rw", "ro"]

class ContainerCollection:
    def create(
        self,
        image: str,
        name: str = ...,
        command: Optional[Union[str, List[str]]] = ...,
        auto_remove: bool = ...,
        network: str = ...,
        volumes: Dict[str, VolumeConfig] = ...,
        environment: Union[List[str], Dict[str, str]] = ...,
        tty: bool = ...,
        links: Dict[str, str] = ...,
        ports: Dict[str, Union[None, int, Tuple[str, int], List[int]]] = ...,
        user: Union[str, int] = ...,
        stdin_open: bool = ...,
    ) -> Container: ...
