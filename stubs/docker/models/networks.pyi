class Network:
    name: str

    def remove(self) -> None:
        ...


class NetworkCollection:
    def create(self, name: str, driver: str = ...) -> Network:
        ...
