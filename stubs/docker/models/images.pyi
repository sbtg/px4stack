import sys
from typing import Optional, overload, List

if sys.version_info > (3, 7):
    from typing import Literal
else:
    from typing_extensions import Literal

class Image:
    pass

class ImageCollection:
    def get(self, name: str) -> Image: ...
    @overload
    def pull(self, repository: str, all_tags: Literal[True]) -> List[Image]: ...
    @overload
    def pull(
        self, repository: str, tag: Optional[str] = ..., all_tags: Literal[False] = ...
    ) -> Image: ...
