FROM ubuntu:focal

ARG MAVSDK_VERSION=0.39.0

RUN DEBIAN_FRONTEND=noninteractive apt update --quiet
RUN DEBIAN_FRONTEND=noninteractive apt install -y wget

WORKDIR /opt/mavsdk_server
RUN wget https://github.com/mavlink/MAVSDK/releases/download/v${MAVSDK_VERSION}/mavsdk_server_manylinux2010-x64
RUN chmod +x ./mavsdk_server_manylinux2010-x64

ENTRYPOINT ["./mavsdk_server_manylinux2010-x64"]

