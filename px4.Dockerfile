FROM px4io/px4-dev-simulation-focal:latest

ENV HEADLESS=1
COPY ./px4.sh /opt/

CMD ["/opt/px4.sh"]

