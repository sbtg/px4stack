PX4Stack
========

Library to manage the lifecycle of a PX4 simulation environment. The behavior of this stack is customizable, but
should work out of the box with the default configuration.

Examples
--------

.. code-block:: python

    from px4stack import Px4Stack, Px4StackConfig

    config = Px4StackConfig()
    prefix = "test_stack"

    with Px4Stack(prefix, config) as stack:
        print(f"Stack online. Logs available at: {stack.log_path}")
