import px4stack.config
import pytest


def test_str() -> None:
    image = px4stack.config.ContainerImage("hello-world", "latest")
    assert str(image) == "hello-world:latest"


def test_converter_with_tag() -> None:
    with_tag = px4stack.config._image_converter("hello-world:1.0")
    assert str(with_tag) == "hello-world:1.0"


def test_converter_without_tag() -> None:
    no_tag = px4stack.config._image_converter("hello-world")
    assert str(no_tag) == "hello-world:latest"


def test_converter_multiple_tags() -> None:
    with pytest.raises(ValueError):
        px4stack.config._image_converter("hello-world:1.0:2.0")


def test_converter_not_strings() -> None:
    with pytest.raises(ValueError):
        px4stack.config._image_converter(10)
        px4stack.config._image_converter({"foo": "bar"})
